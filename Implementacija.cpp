#include <iostream>
#include <string>
#include <ctime>
#include<stdlib.h>
#include<windows.h>
#include "Header.h"


using namespace std;




void Hangman::unos()
{
	int loop = 1;
	char slovo;
	int counter = 0;

	for (int i = 0; i < (int)spremnik.length(); i++)
	{
		if (isalnum(spremnik[i]))
		{
			ispis = ispis + "-";
			counter++;
		}
		else
		{
			ispis = ispis + spremnik[i];
		}
	}
	cout << ispis << endl;

	cout << "Unesite slovo!" << endl;
	cin >> slovo;

	for (int b = 0; b < (int)spremnik.size(); b++)
	{
		if (spremnik[b] == slovo || toupper(slovo) == spremnik[b])
		{
			ispis[b] = slovo;
		}

	}

	cout << ispis << endl;
	cout << "Imate jos " << tries << " pokusaja." << endl;
	while (loop)
	{
		int tracker = 0;
		int brojac = 0;

		cout << "Unesite slovo ili '?' za izlaz" << endl;
		cin >> slovo;

		if (slovo == '?')
		{
			break;
		}
		for (int b = 0; b < (int)spremnik.size(); b++)
		{
			if (spremnik[b] == slovo || toupper(slovo) == spremnik[b])
			{
				ispis[b] = slovo;
				tracker = 1;
			}
			else if (spremnik[b] != slovo || spremnik[b] != toupper(slovo))
			{
				tracker = tracker + 0;
			}
		}

		for (int d = 0; d < counter; d++)
		{
			if (ispis[d] == '-' || ispis[d] == ' ')
			{
				brojac = 1;
			}
			else if (ispis[d] != '-')
			{
				brojac = brojac + 0;
			}
		}

		if (tracker == 0)
		{
			Tries();
			crtez();
		}

		if (brojac == 0 || ispis == spremnik)
		{
			loop = 0;
			cout << "Rijec: " << spremnik << endl;
			cout << "YOU WIN!!!" << endl;
			cout << "Pokusajte ponovno." << endl;
			resetword();
		}
		cout << ispis << endl;

		if (tries > 0 && brojac != 0 && ispis != spremnik)
		{
			cout << "Imate jos " << tries << " pokusaja." << endl;
		}
		if (tries == 0)
		{
			loop = 0;
			cout << "YOU LOSE!" << endl;
			cout << "Rijec je bila: " << spremnik << endl;
			cout << "Pokusajte ponovno" << endl;
			resetword();

			cout << endl;
			cout << endl;
		}

	}
}
void Hangman::save(string x)
{
	spremnik = x;
}
void Hangman::resetword()
{
	spremnik = "";
	ispis = "";
	tries = 7;
}
void Hangman::Tries()
{
	tries--;
}
void Hangman::crtez()
{
	if (tries == 6 || tries == 7)
	{
		cout << " ___________" << endl;
		cout << " |        }" << endl;
		cout << " |         " << endl;
		cout << "_|______________" << endl;
	}
	else if (tries == 5)
	{
		cout << " ___________" << endl;
		cout << " |        }" << endl;
		cout << " |       \\  " << endl;
		cout << "_|______________" << endl;
	}
	else if (tries == 4)
	{
		cout << " ___________" << endl;
		cout << " |         }" << endl;
		cout << " |       \\ 0 " << endl;
		cout << "_|______________" << endl;
	}
	else if (tries == 3)
	{
		cout << " ___________" << endl;
		cout << " |         }" << endl;
		cout << " |       \\ 0 /" << endl;
		cout << "_|______________" << endl;
	}
	else if (tries == 2)
	{
		cout << " ___________" << endl;
		cout << " |        }" << endl;
		cout << " |      \\ 0 /" << endl;
		cout << " |        |" << endl;
		cout << "_|______________" << endl;
	}
	else if (tries == 1)
	{
		cout << " ___________" << endl;
		cout << " |         }" << endl;
		cout << " |       \\ 0 /" << endl;
		cout << " |         | " << endl;
		cout << " |        /  " << endl;
		cout << "_|______________" << endl;
	}
	else
	{
		cout << " ___________" << endl;
		cout << " |         }" << endl;
		cout << " |       \\ 0 /" << endl;
		cout << " |         | " << endl;
		cout << " |        / \\ " << endl;
		cout << "_|______________" << endl;
	}
}
