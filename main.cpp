#include <iostream>
#include <string>
#include <ctime>
#include<stdlib.h>
#include<windows.h>
#include "Header.h"

using namespace std;



int main()
{
	Hangman obj;
	int choice;
	int loop = 1;
	string rijec;
	

	
	while (loop)
	{
		cout << "***************************" << endl;
		cout << "1)UNESITE RIJEC" << endl;//Igra je za 2+ igraca, pa je upisivanje rijeci nevidljivo
		cout << "2)POGADAJTE SLOVA" << endl;
		cout << "3)RESET" << endl;
		cout << "4)QUIT" << endl;
		cout << "***************************" << endl;
		cout << "Vas izbor>";
		cin >> choice;
		if (choice == 1)
		{
			HANDLE hStdin = GetStdHandle(STD_INPUT_HANDLE);
			DWORD mode = 0;
			GetConsoleMode(hStdin, &mode);
			SetConsoleMode(hStdin, mode & (~ENABLE_ECHO_INPUT));
			cout << "Unesite rijec:" << endl;
			cin.ignore();
			getline(cin, rijec);
			SetConsoleMode(hStdin, mode);
			obj.save(rijec);
			cout << "Unijeli ste rijec!" << endl;
			cout << endl;
			cout << endl;
		}

		
		else if (choice == 2)
		{
			obj.unos();
		}
		else if (choice == 3)
		{
			obj.resetword();
			cout << "Dogodio se reset" << endl;
			cout << endl;
		
		}
		else if (choice == 4)
		{
			cout << "Hvala sto ste igrali. :)" << endl;
			loop = 0;
		}
		else
		{
			cout << "ERROR!INVALID CHOICE!" << endl;
			break;
		}

	}

}