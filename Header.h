#include <iostream>
#include <string>
#include <ctime>
#include<stdlib.h>
#include<windows.h>

using namespace std;


class Hangman
{
private:

	int tries = 7;
	int brojac;
	string spremnik;
	string ispis = "";

public:
	void resetword();
	void crtez();
	void unos();
	void save(string Word);
	void Tries();
	
};
